package bean;

import com.User;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Created by admin on 17/6/19.
 */
public class MessageBean {
    private String group;
    private int level;

    private User user;

    private List<String> langs;
    private Set<String> cities;
    private Map<String, Object> score;
    private Properties props;


    public void execute() {
        System.out.println("基本信息_"
                + group + "_" + level);

        if(user != null) {
            System.out.println("bean对象_"
                    + user.getAccount() + "_" + user.getNickname());
        }

        System.out.println("--编程语言：");

        if(langs != null) {
            for(String lang : langs) {
                System.out.println(lang);
            }
        }

        System.out.println("--城市：");

        if(cities != null) {
            for(String city : cities) {
                System.out.println(city);
            }
        }

        System.out.println("--分数：");

        if (score != null) {
            Set<String> set = score.keySet();

            for(String key : set) {
                System.out.println(key + ":" + score.get(key));
            }
        }

        System.out.println("--属性");

        if (props != null) {
            Set<Object> set = props.keySet();

            for(Object key : set) {
                System.out.println(key + ":" + props.get(key));
            }
        }


    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getLangs() {
        return langs;
    }

    public void setLangs(List<String> langs) {
        this.langs = langs;
    }

    public Set<String> getCities() {
        return cities;
    }

    public void setCities(Set<String> cities) {
        this.cities = cities;
    }

    public Map<String, Object> getScore() {
        return score;
    }

    public void setScore(Map<String, Object> score) {
        this.score = score;
    }

    public Properties getProps() {
        return props;
    }

    public void setProps(Properties props) {
        this.props = props;
    }
}
