package example;

import com.User;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by admin on 17/6/23.
 */
@Component
public class Manager {
    @Resource
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
