package example;

import com.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by admin on 17/6/23.
 */

@Component
public class Group {
    private User user;

    @Autowired
    public Group(@Qualifier("user") User user){
        this.user = user;

        System.out.println("group");
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
