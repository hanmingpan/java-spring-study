package example;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by admin on 17/6/22.
 */

@Component("example")
@Scope("singleton")
public class ExampleBean {
    public ExampleBean(){
        System.out.println("construct");
    }

    @PostConstruct
    public void init(){
        System.out.println("init");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("destroy");
    }

    public void execute() {
        System.out.println("execute");
    }
}
