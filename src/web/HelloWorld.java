package web;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by admin on 17/6/28.
 */
@Controller
public class HelloWorld {
    @RequestMapping("/helloWorld")
    public String hello(){
        System.out.println("hello world");

        return "success";
    }
}
