package test;

import bean.MessageBean;
import com.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by admin on 17/6/19.
 */
public class TestInject {
    public static void main(String[] args) {
        TestInject inj = new TestInject();

        inj.test2();
    }

    public void test() {
        String file = "inject.xml";

        ApplicationContext ctx = new ClassPathXmlApplicationContext(file);

        MessageBean bean = ctx.getBean("msg", MessageBean.class);
        bean.execute();
    }

    public void test2() {
        String file = "inject.xml";

        ApplicationContext ctx = new ClassPathXmlApplicationContext(file);

        User bean = ctx.getBean("userDemo", User.class);
        bean.execute();
    }
}
