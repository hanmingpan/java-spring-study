package test;

import com.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Calendar;

/**
 * Created by admin on 17/4/21.
 */
public class Test {
    public static void main(String[] args) {
        Test test = new Test();
//        test.testLoad();
        test.test6();
    }

    public void testLoad(){
        ApplicationContext ctx =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        User user = (User) ctx.getBean("user");

        System.out.println(user);

    }

    public void test2(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        Calendar cal = (Calendar) ctx.getBean("Obj2");


        System.out.println("cal-" + cal);
    }

    public void test3(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        Calendar cal = (Calendar) ctx.getBean("Obj2");
        Calendar cal2 = (Calendar) ctx.getBean("Obj2");


        System.out.println(cal == cal2);
    }

    public void test4(){
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        User user2 = (User)ctx.getBean("user2");
        user2.dance();
        ctx.close();
    }

    public void test5(){
        ApplicationContext ctx =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        User user = (User) ctx.getBean("user3");

        System.out.println(user);

    }

    public void test6(){
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        User user = (User)ctx.getBean("user4", User.class);

        System.out.println(user.getAccount());
    }
}
