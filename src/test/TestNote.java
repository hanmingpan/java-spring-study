package test;

import com.User;
import example.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by admin on 17/6/22.
 */


public class TestNote {
    public static void main(String[] args) {
        TestNote note = new TestNote();

        note.test8();
    }

    public void test(){
        String file = "note.xml";

        ApplicationContext ctx = new ClassPathXmlApplicationContext(file);
        ExampleBean bean = ctx.getBean("example", ExampleBean.class);
        System.out.println(bean);
    }

    public void test2(){
        String file = "note.xml";

        ApplicationContext ctx = new ClassPathXmlApplicationContext(file);
        ExampleBean bean = ctx.getBean("example", ExampleBean.class);
        ExampleBean bean2 = ctx.getBean("example", ExampleBean.class);

        System.out.println(bean == bean2);
    }

    public void test3(){
        String file = "note.xml";

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(file);
        ExampleBean bean = ctx.getBean("example", ExampleBean.class);

        bean.execute();
        ctx.close();
    }

    public void test4(){
        String file = "note.xml";

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(file);
        Group bean = ctx.getBean("group", Group.class);

        System.out.println(bean);
        System.out.println(bean.getUser());
    }

    public void test5(){
        String file = "note.xml";

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(file);
        Company bean = ctx.getBean("company", Company.class);

        System.out.println(bean);
        System.out.println(bean.getUser());
    }

    public void test6(){
        String file = "note.xml";

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(file);
        Organization bean = ctx.getBean("organization", Organization.class);

        System.out.println(bean);
        System.out.println(bean.getUser());
    }

    public void test7(){
        String file = "note.xml";

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(file);
        Manager bean = ctx.getBean("manager", Manager.class);

        System.out.println(bean);
        System.out.println(bean.getUser());
    }

    public void test8(){
        String file = "note.xml";

        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext(file);
        User bean = ctx.getBean("user", User.class);

        System.out.println(bean.getDelFlag());
    }
}
