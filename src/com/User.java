package com;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by admin on 17/3/29.
 */

@Component
public class User implements Serializable {
    private int id;
    private String account;
    private String password;
    private String nickname;
    private Date createTime;
    private Date lastVisit;
    @Value("#{const.PAGE_SIZE}")
    private int delFlag;

    public User(){
        super();
        System.out.println("user create");
    }

    public User(String account,String nickname) {
        this.account = account;
        this.nickname = nickname;
    }

    public User(int id, String account, String password, String nickname, Date createTime,
                Date lastVisit, int delFlag) {
        this.id = id;
        this.account = account;
        this.password = password;
        this.nickname = nickname;
        this.createTime = createTime;
        this.lastVisit = lastVisit;
        this.delFlag = delFlag;
    }

    public void execute(){
        System.out.println(account + "-" + nickname);
    }

    public void init(){
        System.out.println("user init");
    }

    public void destroy(){
        System.out.println("user destroy");
    }

    public void dance(){
        System.out.println("user dance");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(Date lastVisit) {
        this.lastVisit = lastVisit;
    }

    public int getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(int delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString(){
        return "{'account' : '" + account + "','nickname':'" + nickname + "'}";
    }
}
